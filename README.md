## A simple chrome plugin - helper for web grader 

### Install
- drag `Helper_Grader_COMP410.crx` to the chrome extension page to add the plugin
- or you can load unpacked extension folder under developer mode

### Functionality
1. Blue the students with 'NA', added button in project page to find the next ungraded student and start grading
2. Added button to (automatically) grade all features
3. Added 'I feel all right' button, if the students got all right in all automatic grading features then make the score all right.
4. highlight specific code segment in code view
5. add a shortcut button 'save&continue' on the side

### Implementation Note
- inject content script 
- jquery selector, setTimeout, and simulate click() button
