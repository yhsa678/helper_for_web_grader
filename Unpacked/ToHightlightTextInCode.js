"use strict";

//$('body').hightlight('int');
//$(document).ajaxComplete(function() {
//	console.log('here');
$(document).ready(
	function() {
		var option = JSON.parse(localStorage.getItem(option_name));
		var code_highlight = JSON.parse(option.code_highlight);
		setInterval(function() {
			//$("#code").highlightRegex(/new/ig);
			//$("#code").highlight(/new.*int.*\[.*\]/ig);
			// $("#code").highlightRegex(/list/ig);
			// $("#code").highlightRegex(/link/ig);
			// $("#code").highlightRegex(/next/ig);
			// $("#code").highlightRegex(/lookupNode/ig);
			for (var i = 0; i < code_highlight.length; ++i) {
				$("#code").highlightRegex(new RegExp(code_highlight[i], "ig"));
			}
		}, 1000);
	}
);