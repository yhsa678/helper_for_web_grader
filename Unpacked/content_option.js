"use strict";
// this file is to handle option settings in content scripts

var default_option = {
	findnext_start: "-1",
	findnext_end: "-1",
	autograding: "0",
	autocheck: "[]",
	code_highlight: '["new", "next"]'
};

var option_name = "option";

(function() {
	if (null === localStorage.getItem(option_name)) {
		localStorage.setItem(option_name, JSON.stringify(default_option));
	}
})();

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		console.log(sender.tab ?
			"from a content script:" + sender.tab.url :
			"from the extension: pop-up page");
		if (request.type == "load_option") {
			sendResponse(JSON.parse(localStorage.getItem(option_name)));
		} else if (request.type == "save_option") {
			localStorage.setItem(option_name, JSON.stringify(request.option));
			sendResponse({
				status: true
			});
		}
	}
);

chrome.runtime.sendMessage({
		type: "show_page_action"
	});