// create grade all button
$("div.tab-pane.active td.ng-binding").eq(-1).after('<button class="btn btn-primary" id="grade-all" style="background-color: red;margin-top: 5px;">Grade All</button>');

// click all grade buttons
function TestClick()
{
	var option = JSON.parse(localStorage.getItem(option_name));
	var autocheck = JSON.parse(option.autocheck);

	var x = $("div.tab-pane.active button.btn.btn-primary.btn-sm");
	for (var i=0; i<x.length; ++i) {
		if (autocheck.indexOf(i) >= 0) continue;
		setTimeout('$("div.tab-pane.active button.btn.btn-primary.btn-sm").eq('+i+').click()', 200*i);
	}
	//setTimeout('CheckScores();', x.length*i+500);
	//setTimeout('CheckScores();', x.length*i+1000);
	//setTimeout('ContNextIfAllRight();', x.length*i+1000);
	//setTimeout('ContNextIfAllRight();', x.length*i+2000);
	setTimeout(function() {
		var total = $("td.ng-binding").has('strong').eq(0).text();
		// if not 0, give the clean code points
		if (total != "Total: 0") { 
			AutoPoints();
		}
	}, x.length*i+1000);
	return;
}

$("#grade-all").on("click", TestClick);

// try once or twice after load the page
$(document).ready(function() {
	var option = JSON.parse(localStorage.getItem(option_name));
	var autograding = JSON.parse(option.autograding);
	for (var i=0; i<autograding; ++i) {
		setTimeout('TestClick();', 50 + 1000*i);
	}
});

//$(document).ready(setTimeout('TestClick();', 2500));
//setTimeout('TestClick();', 500);
//setTimeout('TestClick();', 2500);

// add for fully automatic grading, stop only if a zero happen
// $(document).ready(setTimeout(function() {
// 	var total = $("td.ng-binding").has('strong').eq(0).text();
// 	if (total != "Total: 0")
// 		$('#save-cont').click();
// 		//$('button[ng-click="continue()"]').click();
// }, 10000));

// ====================================
function AutoPoints() {
	var option = JSON.parse(localStorage.getItem(option_name));
	var autocheck = JSON.parse(option.autocheck);
	for (var i=0; i<autocheck.length; ++i) {
		$('button.btn.btn-success.btn-xs[ng-click="fullCredit(feature)"]').eq(autocheck[i]).click();
	}
}

// ====================================
function ContNextIfAllRight() {
	var total = $("td.ng-binding").has('strong').eq(0).text();
	// if not 0, give the clean code points
	if (total != "Total: 0") {
		//$('button.btn.btn-success.btn-xs[ng-click="fullCredit(feature)"]').eq(2).click()
		//$('button.btn.btn-success.btn-xs[ng-click="fullCredit(feature)"]').eq(4).click()
		AutoPoints();
	}

	if (total.indexOf('100')>0) {
		//$('button.btn.btn-success.btn-xs[ng-click="fullCredit(feature)"]').eq(-2).click()
		$('button[ng-click="continue()"]').click();
	}
}

// add feel all right button
$('#grade-all').after('<button class="btn btn-primary" id="all-right" style="background-color: purple;margin-top: 5px;margin-left: 5px;">I feel all right!</button>');

//$('#all-right').css('display', 'none');
$('#all-right').on("click", ContNextIfAllRight);


// =====================================
// try to check the score field and show the error row
// however, on-change event don't work... maybe need use Angular ng-change event to get instant response
setInterval(CheckScores, 600);
function CheckScores() {
	var pts = $("div#features td.ng-binding").filter(":odd");
	var ss = $('input[ng-model="feature.result.score"]');
	var frow = $('#features table tr.ng-scope');
	for (var i=0; i<pts.length; ++i) {
		if (pts.eq(i).text() != ss.eq(i).val()) {
			frow.eq(i).children('td,th').css('background-color', 'rgb(225, 255, 137)');
		}
		else {
			frow.eq(i).children('td,th').css('background-color', '');
		}
	}
}

// =======================================
// duplicate a save&continue button
$('#all-right').after('<button class="btn btn-primary" id="save-cont" style="background-color: rgb(17, 124, 0);margin-top: 5px;margin-left: 5px;">Save & Continue</button>');
$('#save-cont').click(function() {$('button[ng-click="continue()"]').click();});
