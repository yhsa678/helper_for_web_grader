// create a containsIN that is case insensitive search
$.extend($.expr[":"], {
"containsIN": function(elem, i, match, array) {
return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
}
});

// create text field to search
$("#start").after('<input type="text" id="search_stu" style="width: 64px; height: 31px; margin-left: 10px;">');
$('#search_stu').keyup(function() {
	var word = this.value;
	var se = $("#students").find('option:containsIN("'+word+'")');
	
	if (se.length != 0) {
		se.eq(0).attr("selected", true).css('color', 'red');
		$("#search_response").text("");
	}
	else {
		$("#search_response").text("Not found!");
	}
});
// create button
// $("#start").eq(-1).after('<button class="btn btn-primary" id="find_next" style="background-color: red;margin-left: 10px;">find next ungraded and start grading</button>');
$("#search_stu").after('<button class="btn btn-primary" id="find_next" style="background-color: red;margin-left: 10px;">find next ungraded and start grading</button>');
// add response field for search
$("#find_next").after("<p id='search_response'><strong></strong></p>");
$("#search_response").after("<p id='find_ungraded_response'><strong></strong></p>");

// make ungraded student's name blue
$(document).ready(function() {
	var nongraded = $("#students").find('option:containsIN("[NA]")');
	for (var i=0; i<nongraded.length; ++i) {
		nongraded.eq(i).css('color', 'blue');
	}

	var totalNum = $("#students")[0].length;
	var gradedNum = totalNum - nongraded.length;
	$("h2").text($("h2").text().concat(' ('+gradedNum.toString()+'/'+totalNum.toString()+')'));
});

// find next ungraded students
function findnextClick()
{
	var option = JSON.parse(localStorage.getItem(option_name));
	var fnst = option.findnext_start;
	var fned = option.findnext_end;
	if (fnst == -1) {
		var nongraded = $("#students").find('option:containsIN("[NA]")');
		if (nongraded.length != 0) {
			nongraded.eq(0).attr("selected", true);
			$("#start").click();
		}
		else {
			$("#find_ungraded_response").text("You've finished~~");
		}
	} else {
		// if has range, use a naive way...
		var stu = $("#students option").slice(fnst, fned);
		for (var i=0; i<stu.length; ++i) {
			if (stu.eq(i).text().indexOf("[NA]") > 0) {
				stu.eq(i).attr("selected", true);
				$("#start").click();
				return;
			}
		}
		$("#find_ungraded_response").text("You've finished your duty~~");
	}
}

$("#find_next").on("click", findnextClick);






