"use strict";

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		console.log(sender.tab ?
			"from a content script:" + sender.tab.url :
			"from the extension: pop-up page");
		if (sender.tab && request.type == "show_page_action") {
			chrome.pageAction.show(sender.tab.id);
		}
	}
);