$(document).ready(function() {
	// save option when click save
	$('#option_save').click(function() {
		chrome.tabs.query({
			active: true,
			currentWindow: true
		}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, {
				type: "save_option",
				option: {
					findnext_start: $('#findnext_start')[0].value,
					findnext_end: $('#findnext_end')[0].value,
					autograding: $('#autograding')[0].value,
					autocheck: $('#autocheck')[0].value,
					code_highlight: $('#code_highlight')[0].value
				}
			}, function(response) {
				//console.log(response);
				if (response.status === true) {
					// $('#option_save').append('<p style="color:red;">Option saved!</p>');
					$('#save_status').eq(0).text("Option saved!");
				}
			});
		});
	});

	// load option when popup
	chrome.tabs.query({
		active: true,
		currentWindow: true
	}, function(tabs) {
		chrome.tabs.sendMessage(tabs[0].id, {
			type: "load_option"
		}, function(response) {
			console.log(response);
			$('#findnext_start')[0].value = response.findnext_start;
			$('#findnext_end')[0].value = response.findnext_end;
			$('#autograding')[0].value = response.autograding;
			$('#autocheck')[0].value = response.autocheck;
			$('#code_highlight')[0].value = response.code_highlight;
		});
	});
});